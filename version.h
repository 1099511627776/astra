#ifndef _VERSION_H_
#define _VERSION_H_ 1

#define ASTRA_VERSION_MAJOR 4
#define ASTRA_VERSION_MINOR 0
#define ASTRA_VERSION_DEV 272

#endif /* _VERSION_H_ */
